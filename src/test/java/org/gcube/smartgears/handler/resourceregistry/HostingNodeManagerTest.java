package org.gcube.smartgears.handler.resourceregistry;

import java.lang.management.ManagementFactory;
import java.util.Map;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.gcube.resourcemanagement.model.reference.entities.facets.SoftwareFacet;
import org.gcube.smartgears.handler.resourceregistry.resourcemanager.HostingNodeManager;
import org.gcube.smartgears.handler.resourceregistry.resourcemanager.LinuxDistributionInfo;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HostingNodeManagerTest {

	private static final Logger logger = LoggerFactory.getLogger(HostingNodeManagerTest.class);
	
	@Test
	public void testOSAndMemInfo() throws Exception {
		logger.debug("Runtime.getRuntime().maxMemory() : {}", Runtime.getRuntime().maxMemory());
		logger.debug("Runtime.getRuntime().totalMemory() : {}", Runtime.getRuntime().totalMemory());
		logger.debug("Runtime.getRuntime().freeMemory() : {}", Runtime.getRuntime().freeMemory());
		
		long allocatedMemory = (Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory());
		long presumableFreeMemory = Runtime.getRuntime().maxMemory() - allocatedMemory;
		logger.debug("allocatedMemory : {}", allocatedMemory);
		logger.debug("presumableFreeMemory : {}", presumableFreeMemory);
		
		MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
		Object total = mBeanServer.getAttribute(new ObjectName("java.lang","type","OperatingSystem"), "TotalPhysicalMemorySize");
		logger.debug("Total Memory : {}", total);
		
		Object free = mBeanServer.getAttribute(new ObjectName("java.lang","type","OperatingSystem"), "FreePhysicalMemorySize");
		logger.debug("Free Memory : {}", free);
		
		/* Keep this code to performs checks */
		java.lang.management.OperatingSystemMXBean mxbean = ManagementFactory.getOperatingSystemMXBean();
		@SuppressWarnings("restriction")
		com.sun.management.OperatingSystemMXBean sunmxbean = (com.sun.management.OperatingSystemMXBean) mxbean;
		@SuppressWarnings("restriction")
		long freeMemory = sunmxbean.getFreePhysicalMemorySize();
		@SuppressWarnings("restriction")
		long totalMemory = sunmxbean.getTotalPhysicalMemorySize();
		logger.debug("freeMemory : {}", freeMemory);
		logger.debug("totalMemory : {}", totalMemory);
		
		
		logger.debug(mxbean.getName()); // softwareFacet.setGroup(System.getProperty("os.name"));
		logger.debug(mxbean.getArch()); // softwareFacet.setName(System.getProperty("os.arch"));
		logger.debug(mxbean.getVersion()); // softwareFacet
		
		logger.debug(System.getProperty("os.name"));
		logger.debug(System.getProperty("os.arch"));

	}
	
	@Test
	public void testGetLinuxDistributionInfo() {
		LinuxDistributionInfo linuxDistributionInfo = new LinuxDistributionInfo();
		Map<String,String> map = linuxDistributionInfo.getInfo();
		logger.info("{}", map);
         
	}
	
	@Test
	public void testGetOSFacet() {
		SoftwareFacet softwareFacet = HostingNodeManager.getOperativeSystem();
		logger.info("{}", softwareFacet);
         
	}
}
