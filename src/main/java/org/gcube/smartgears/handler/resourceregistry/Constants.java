package org.gcube.smartgears.handler.resourceregistry;

import org.gcube.smartgears.handler.resourceregistry.resourcemanager.HostingNodeManager;

/**
 * Library-wide constants.
 * @author Luca Frosini
 * @author Lucio Lelii
 */
public class Constants {

	public static final String HOSTING_NODE_MANAGER_PROPERTY = HostingNodeManager.class.getSimpleName();
	
//	/**
//	 * The name of the context property that contains the EService Resource.
//	 */
//	public static final String ESERVICE_MANAGER_PROPERTY = EServiceManager.class.getSimpleName();
	
	/**
	 * The configuration name of {@link EServiceHandler} and {@link HostingNodeHandler}.
	 */
	public static final String RESOURCE_MANAGEMENT = "resource-management";
	
	public static final long default_container_publication_frequency_in_seconds = 60;
	
	public static final int application_republish_frequency_in_minutes = 20;
	
}
