package org.gcube.smartgears.handler.resourceregistry;

import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;

import org.gcube.common.authorization.client.proxy.AuthorizationProxy;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.informationsystem.resourceregistry.api.contexts.ContextCache;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.smartgears.provider.ProviderFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI-CNR)
 */
public class ContextUtility {

	private static Logger logger = LoggerFactory.getLogger(ContextUtility.class);

	private static AuthorizationProxy authorizationProxy;

	static {
		authorizationProxy = ProviderFactory.provider().authorizationProxy();
	}

	public static void resetContex() {
		SecurityTokenProvider.instance.reset();
		ScopeProvider.instance.reset();
	}

	public static void setContextFromToken(String token) {
		if (token == null || token.compareTo("") == 0) {
			resetContex();
		} else {
			SecurityTokenProvider.instance.set(token);
			String scope = getContextName(token);
			ScopeProvider.instance.set(scope);
		}

	}

	public static String getCurrentContextName() {
		String token = SecurityTokenProvider.instance.get();
		return getContextName(token);
	}
	
	public static UUID getContextUUID(String token) throws ResourceRegistryException {
		ContextCache contextCache = ContextCache.getInstance();
		String contextFullName = getContextName(token);
		UUID contextUUID = contextCache.getUUIDByFullName(contextFullName);
		return contextUUID;
	}
		
	public static String getContextName(String token) {
		try {
			return authorizationProxy.get(token).getContext();
		} catch (Exception e) {
			logger.error("Error retrieving context form token {}, it should never happen", token, e);
			return null;
		}
	}
	
	public static SortedSet<String> getContextFullNamesFromTokens(Set<String> tokens){
		SortedSet<String> contextFullNames = new TreeSet<>();
		for(String token : tokens) {
			String contextFullName = getContextName(token);
			contextFullNames.add(contextFullName);
		}
		return contextFullNames;
	}
	
	public static SortedSet<UUID> getContextUUIDFromTokens(Set<String> tokens) throws ResourceRegistryException {
		SortedSet<UUID> contextsUUID = new TreeSet<>();
		ContextCache contextCache = ContextCache.getInstance();
		for(String token : tokens) {
			String contextFullName = getContextName(token);
			UUID contextUUID = contextCache.getUUIDByFullName(contextFullName);
			contextsUUID.add(contextUUID);
		}
		return contextsUUID;
	}

	public static String getContextNameFromUUID(UUID contextUUID) throws ResourceRegistryException {
		ContextCache contextCache = ContextCache.getInstance();
		return contextCache.getContextFullNameByUUID(contextUUID);
	}
	
}
