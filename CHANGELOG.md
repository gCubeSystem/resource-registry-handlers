This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Resource Registry Handlers

## [v2.3.0]

- Migrate code to reorganized E/R format [#24992]


## [v2.2.0]

- Enhanced gcube-smartgears-bom version


## [v2.1.0]

- Aligned model with the model defined in Luca Frosini PhD dissertation [#20367]
- Added support for context names included in header among UUIDs [#22090]


## [v2.0.0]

- Refactored Handlers to harmonize and make more efficient the code [#19997]
- Switched JSON management to gcube-jackson [#19116]


## [v1.3.0] [r4.21.0] - 2020-03-30

- Refactored code to support IS Model reorganization (e.g naming, packages)
- Fixed import due to gcube model refactoring
- Refactored code to support renaming of Embedded class to Property [#13274]


## [v1.2.0] [r4.13.0] - 2018-11-20

- Using resource-registry v2 APIs [#11950]


## [v1.1.0] [r4.9.0] - 2017-12-20

- Creating uber-jar instead of jar-with-dependencies and using new make-servicearchive directive [#10166]
- Using new APIs signature [#10318]


## [v1.0.0] [r4.6.0] - 2017-07-25

- First Release

